from django.db import models

# Create your models here.
class Note(models.Model):
    to_name = models.CharField(max_length=20)
    from_name = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    message = models.CharField(max_length=200)
