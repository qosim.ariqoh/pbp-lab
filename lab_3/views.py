from django.http import response
from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {
        'page_title': 'Friend List',
        'friends': friends
    }
    return render(request, 'lab3_index.html' , response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    friend = FriendForm(request.POST or None)

    if request.method == "POST" :
        if friend.is_valid():
            friend.save()
            return redirect("index3")

    response = {
        'page_title': 'Friend Form',
        'friend_form': friend
    }

    return render(request, 'lab3_form.html', response)