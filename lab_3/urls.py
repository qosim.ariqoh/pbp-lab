from collections import namedtuple
from django.urls import path
from django.contrib.auth import views as auth_views
from .views import index, add_friend

urlpatterns = [
    path('', index, name="index"),
    path('add/', add_friend, name="friend form"),
    path('admin/login', auth_views.LoginView.as_view()),
]