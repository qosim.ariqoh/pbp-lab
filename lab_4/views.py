from django.http import request, response
from django.shortcuts import redirect, render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
footer_msg = 'Nothing here'
css_url = 'lab_4/css/index.css'

def index(request):
    notes = Note.objects.all().values()
    response = {
        'app_css': css_url,
        'page_title': 'Note - Table',
        'footer': footer_msg,
        'notes': notes,
    }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    notes = NoteForm(request.POST or None)

    response = {
        'app_css': css_url,
        'page_title': 'Note - Form',
        'footer': footer_msg,
        'note_form': notes,
    }


    if request.method == "POST" :
        if notes.is_valid():
            notes.save()
            return redirect('index4')

    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {
        'app_css': css_url,
        'page_title': 'Note - List',
        'footer': footer_msg,
        'notes': notes,
    }

    return render(request, 'lab4_note_list.html', response)